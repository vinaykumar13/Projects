# Projects
Contains Two Small projects:
	1. Age Calculator
	2. YouTube Downloader

Both the projects are done using Python GUI

The Projects are very simple and basic.

1. Age calculator calculates age from date of birth and displays age in 'Days', 'Years' and an upcoming Lucky day(Just for Fun).

2. YouTube Downloader downloads progressive videos upto "720p". just provide the URL and the video quality option.
