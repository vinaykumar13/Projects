import calendar

import time

import os

import string

import datetime

from datetime import date

from tkinter import *


window=Tk()

screen_width = window.winfo_screenwidth()

screen_height = window.winfo_screenheight()

window.configure(height=screen_height)

window.configure(width=screen_width)

window.title("Age calculator")

#bg_img=PhotoImage(file="bg.png")

#Label(window,image=bg_img).place(x=0,y=0,relwidth=1,relheight=1)

#window.configure(bg="#3b3a36")

window.configure(bg="#06000a")


#function to calculate recursive sum of all digits
def rec_sum(n):
        rec_sum=0
        while(n>0 or rec_sum>9):
                if n==0:
                        n=rec_sum
                        rec_sum=0
                s=n%10
                rec_sum+=s
                n//=10
        return rec_sum


#function to calculate no of days since birth
def ndays():

        text2display=""

        name_no_list=sum([ord(i.upper()) for i in name_in.get() if i!=' '])

        input_date=[year_in.get(),month_in.get(),date_in.get()]

        if (input_date[1]<=12 and input_date[1]>=1 and input_date[2]<=31 and input_date[2]>=1):
                #today is retrieved and stored as a list
                tdy_yr,tdy_mnth,tdy_date=datetime.datetime.today().strftime("%Y %m %d").split()
                
                present_day=date(int(tdy_yr),int(tdy_mnth),int(tdy_date))
                #no of leap years from bday
                tot_leap_years=calendar.leapdays(int(input_date[0]),int(datetime.datetime.today().strftime("%Y")))

                bday=date(input_date[0],input_date[1],input_date[2])

                date_diff=present_day-bday

                no_days=(date_diff.days-tot_leap_years)//365

                text2display=str(date_diff.days)+" Days\t\t"+str(no_days)+" Years"

                lucky_no_diff=abs(rec_sum(name_no_list)-rec_sum(date_diff.days))

                text2display+="\n\nYour Luck day is "+str(present_day+datetime.timedelta(lucky_no_diff))

        else:

                text2display="Please Enter a valid Date\t\t\n\n\t\t\t"
        
        Label(window,text=text2display,height=7,width=70,bg="#c0dfd9").place(x=(screen_width//3),y=(screen_height//4)+350)



date_in=IntVar()

month_in=IntVar()

year_in=IntVar()

name_in=StringVar()

Label(window,text="Name", width=20,bg="#c0dfd9").place(x=screen_width//3,y=screen_height//4)

ent1=Entry(window,textvariable=name_in,width=50,bg="#e9ece5")

ent1.focus()

ent1.place(x=(screen_width//3)+190,y=screen_height//4)

Label(window,text="Date", width=20,bg="#c0dfd9").place(x=screen_width//3,y=(screen_height//4)+50)

Entry(window,textvariable=date_in,width=50,bg="#e9ece5").place(x=(screen_width//3)+190,y=(screen_height//4)+50)

Label(window,text="Month", width=20,bg="#c0dfd9").place(x=screen_width//3,y=(screen_height//4)+100)

Entry(window,textvariable=month_in,width=50,bg="#e9ece5").place(x=(screen_width//3)+190,y=(screen_height//4)+100)

Label(window,text="Year", width=20,bg="#c0dfd9").place(x=screen_width//3,y=(screen_height//4)+150)

Entry(textvariable=year_in,width=50,bg="#e9ece5").place(x=(screen_width//3)+190,y=(screen_height//4)+150)

Button(window,text="Exit",width=25,bg="#b3c2bf",command=quit).place(x=(screen_width//3)+20,y=(screen_height//4)+250)

Button(window,text="Submit",width=25,bg="#b3c2bf",command=ndays).place(x=(screen_width//3)+270,y=(screen_height//4)+250)


window.mainloop()
