
from pytube import YouTube

import os

from tkinter import *


window=Tk()

screen_width = window.winfo_screenwidth()

screen_height = window.winfo_screenheight()

window.configure(height=screen_height)

window.configure(width=screen_width)

window.title("YouTube Downloader")

#bg_img=PhotoImage(file="bg.png")

#Label(window,image=bg_img).place(x=0,y=0,relwidth=1,relheight=1)

window.configure(bg="red")


def yt_dwnld():
    
    url=url_in.get().strip()

    #name=name_in.get().strip()
    try:
        yt=YouTube(url)
    except:
        txt_dis="Invalid Video \t\t\t\t\t\t\t\t "
    
    #txt2=yt.streams.filter(progressive=True, file_extension='mp4').order_by("resolution")[-1]
    else:
        f1=yt.streams.filter(progressive=True, file_extension='mp4', resolution=res_in.get()).all()
        
        if len(f1)==0:
            txt2=yt.streams.filter(progressive=True, file_extension='mp4').all()
            txt_dis="Selected Resolution Not Available, Please Select a Lower Resolution "+str([i.resolution for i in txt2])
        else:
            txt_dis="Please Wait...  \tDownloading.....\t\t\t\t\t\t\t\t"
            #yt.streams.filter(progressive=True, file_extension='mp4', resolution=res_in.get()).first().download("C:\\Users\\vinay\\Downloads")
            try:
                f1[0].download("C:\\Users\\vinay\\Downloads")
            except:
                txt_dis="Download Error\t\t\t\t\t\t\t\t\t\t"
    Label(window,text=str(txt_dis)).place(x=(screen_width//3)+20,y=(screen_height//3)+300)



url_in=StringVar()

res_in=StringVar()

Label(window,text="URL", width=20).place(x=screen_width//3,y=screen_height//3)

ent1=Entry(window,textvariable=url_in,width=50)

ent1.focus()

ent1.place(x=(screen_width//3)+200,y=screen_height//3)

r1=Radiobutton(window,text="720p",variable=res_in,value="720p",width=8)

r1.select()

r1.place(x=(screen_width//3),y=(screen_height//3)+100)

Radiobutton(window,text="480p",variable=res_in,value="480p",width=8).place(x=(screen_width//3)+110,y=(screen_height//3)+100)

Radiobutton(window,text="360p",variable=res_in,value="360p",width=8).place(x=(screen_width//3)+215,y=(screen_height//3)+100)

Radiobutton(window,text="240p",variable=res_in,value="240p",width=8).place(x=(screen_width//3)+320,y=(screen_height//3)+100)

Radiobutton(window,text="144p",variable=res_in,value="144p",width=8).place(x=(screen_width//3)+425,y=(screen_height//3)+100)


Button(window,text="Exit",width=25,command=quit).place(x=(screen_width//3)+20,y=(screen_height//3)+200)

Button(window,text="Submit",width=25,command=yt_dwnld).place(x=(screen_width//3)+270,y=(screen_height//3)+200)


window.mainloop()
